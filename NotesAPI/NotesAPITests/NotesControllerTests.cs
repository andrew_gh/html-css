using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Mvc.Routing;
using Moq;
using NotesAPI.Controllers;
using NotesAPI.Entities;
using NotesAPI.Helpers;
using NotesAPI.Models;
using NotesAPI.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NotesAPITests
{
    public class Tests
    {
        private Mock<INoteCollectionService> _mockNoteCollectionService;
        private Mock<IMapper> _mockMapper;
        private NotesController _notesController;
        private Mock<IUrlHelper> _mockUrlHelper;
        private Mock<ITypeHelperService> _mockTypeHelperService;
        private Mock<IUrlHelperFactory> _mockUrlHelperFactory;
        private Mock<IActionContextAccessor> _mockActionContextAccessor;

        [SetUp]
        public void Setup()
        {
            _mockNoteCollectionService = new Mock<INoteCollectionService>();
            _mockTypeHelperService = new Mock<ITypeHelperService>();
            _mockUrlHelperFactory = new Mock<IUrlHelperFactory>();
            _mockActionContextAccessor = new Mock<IActionContextAccessor>();
            _mockMapper = new Mock<IMapper>();
            _mockUrlHelper = new Mock<IUrlHelper>();
            _mockUrlHelperFactory.Setup(m => m.GetUrlHelper(It.IsAny<ActionContext>())).Returns(_mockUrlHelper.Object);
            _notesController = new NotesController(_mockNoteCollectionService.Object, _mockTypeHelperService.Object,
                _mockUrlHelperFactory.Object, _mockActionContextAccessor.Object);

            var objectValidator = new Mock<IObjectModelValidator>();
            objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                              It.IsAny<ValidationStateDictionary>(),
                                              It.IsAny<string>(),
                                              It.IsAny<Object>()));
            _notesController.ObjectValidator = objectValidator.Object;

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Note, NoteDto>()
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => CategoryExtension.ConvertCategory(src.CategoryId)))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Title));

                cfg.CreateMap<NoteForUpdateDto, Note>();

                cfg.CreateMap<NoteForPatchDto, Note>()
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => CategoryExtension.ConvertCategory(src.Category)))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Name));

                cfg.CreateMap<Note, NoteForPatchDto>()
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => CategoryExtension.ConvertCategory(src.CategoryId)))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Title));
            });
        }

        [Test]
        public async Task CreateNoteWhenNoteIsNullTest()
        {
            var result = await _notesController.Create(null).ConfigureAwait(false);
            var resultValue = ((BadRequestObjectResult)result).Value;

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
            Assert.That(resultValue, Is.EqualTo("Note cannot be null"));
        }

        [Test]
        public async Task CreateValidNoteTest()
        {
            Note note = new Note() { Id=Guid.NewGuid(),Title="test note", OwnerId=Guid.NewGuid()};
            _mockNoteCollectionService.Setup(x => x.Create(It.IsAny<Note>())).ReturnsAsync(true).Verifiable();

            var result = await _notesController.Create(note).ConfigureAwait(false);
            var resultValue = ((CreatedAtRouteResult)result).Value;

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<CreatedAtRouteResult>(result);
            Assert.That(resultValue, Is.EqualTo(note));
        }

        [Test]
        public async Task GetNoteByIdTestWhenIdNotFoundTest()
        {
            _mockNoteCollectionService.Setup(x => x.Get(It.IsAny<Guid>())).ReturnsAsync((Note)null).Verifiable();
            
            var result = await _notesController.Get(Guid.NewGuid()).ConfigureAwait(false);
            var resultValue = ((NotFoundObjectResult)result).Value;

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<NotFoundObjectResult>(result);
            Assert.That(resultValue, Is.EqualTo("There is no note with the given Id"));
        }

        [Test]
        public async Task GetNoteByValidIdTest()
        {
            var id = Guid.NewGuid();
            _mockNoteCollectionService.Setup(x => x.Get(It.IsAny<Guid>())).ReturnsAsync(new Note() { Id = id}).Verifiable();
            _mockUrlHelper.Setup(x => x.Link("GetNote", id)).Returns("Notes/"+id.ToString());

            var result = await _notesController.Get(id).ConfigureAwait(false);
            var resultValue = ((OkObjectResult)result).Value;

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.That(id, Is.EqualTo(((NoteDto)resultValue).Id));
        }

        [Test]
        public async Task GetAllNotesWhenNoteResourceParametersAreInvalidTest()
        {
            NoteResourceParameters val = new NoteResourceParameters() { Fields = "ceva invalid" };
            _mockNoteCollectionService.Setup(x => x.GetAll()).ReturnsAsync(new List<Note>
                (new Note[] { new Note() { Id = Guid.NewGuid(), Title = "TestNote1" },
                new Note() { Id = Guid.NewGuid(), Title = "TestNote2" } })).Verifiable();
            _mockTypeHelperService.Setup(x => x.TypeHasProperties<NoteDto>(It.IsAny<string>())).Returns(false).Verifiable();

            var result = await _notesController.GetAll(val).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public async Task GetAllNotesWhenNoNoteResourceParametersTest()
        {
            _mockNoteCollectionService.Setup(x => x.GetAll()).ReturnsAsync(new List<Note>
                (new Note[] { new Note() { Id = Guid.NewGuid(), Title = "TestNote1" },
                new Note() { Id = Guid.NewGuid(), Title = "TestNote2" } })).Verifiable();

            var result = await _notesController.GetAll().ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task GetAllNotesWhenGivenNoteResourceParametersTest()
        {
            NoteResourceParameters val = new NoteResourceParameters() { Fields = "Id" };
            _mockNoteCollectionService.Setup(x => x.GetAll()).ReturnsAsync(new List<Note>
                (new Note[] { new Note() { Id = Guid.NewGuid(), Title = "TestNote1" },
                new Note() { Id = Guid.NewGuid(), Title = "TestNote2" } })).Verifiable();
            _mockTypeHelperService.Setup(x => x.TypeHasProperties<NoteDto>(It.IsAny<string>())).Returns(true).Verifiable();

            var result = await _notesController.GetAll(new NoteResourceParameters() { Fields = "Id" }).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task DeleteNoteSuccessTest()
        {
            _mockNoteCollectionService.Setup(x => x.Delete(It.IsAny<Guid>())).ReturnsAsync(true).Verifiable();
            
            var result = await _notesController.Delete(Guid.NewGuid()).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task DeleteNoteTest()
        {
            _mockNoteCollectionService.Setup(x => x.Delete(It.IsAny<Guid>())).ReturnsAsync(false).Verifiable();
            
            var result = await _notesController.Delete(Guid.NewGuid()).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<NotFoundObjectResult>(result);
        }

        [Test]
        public async Task UpdateNoteSuccessTest()
        {
            _mockNoteCollectionService.Setup(x => x.Update(It.IsAny<Guid>(), It.IsAny<Note>())).ReturnsAsync(true).Verifiable();
            
            var result = await _notesController.UpdateNote(Guid.NewGuid(), new Note() { Id = Guid.NewGuid() }).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<OkObjectResult>(result);
        }

        [Test]
        public async Task UpdateNoteWhenIdIsNotFoundTest()
        {
            _mockNoteCollectionService.Setup(x => x.Update(It.IsAny<Guid>(), It.IsAny<Note>())).ReturnsAsync(false).Verifiable();
            
            var result = await _notesController.UpdateNote(Guid.NewGuid(), new Note() { Id = Guid.NewGuid() }).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<OkResult>(result);
        }

        [Test]
        public async Task UpdateNoteWhenIdIsNullTest()
        {
            _mockNoteCollectionService.Setup(x => x.Update(It.IsAny<Guid>(), It.IsAny<Note>())).ReturnsAsync(false).Verifiable();
            
            var result = await _notesController.UpdateNote(Guid.NewGuid(), null).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<BadRequestObjectResult>(result);
        }

        [Test]
        public async Task PatchWhenPatchDocIsNullTest()
        {
            var result = await _notesController.PartiallyUpdateNote(Guid.NewGuid(), null);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public async Task PatchWhenNoteDoesNotExistTest()
        {
            _mockNoteCollectionService.Setup(x => x.NoteExists(It.IsAny<Guid>())).ReturnsAsync(false).Verifiable();
            
            var result = await _notesController.PartiallyUpdateNote(Guid.NewGuid(), new JsonPatchDocument<NoteForPatchDto>()).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task PatchWhenGetNoteReturnsNullTest()
        {
            _mockNoteCollectionService.Setup(x => x.Get(It.IsAny<Guid>())).ReturnsAsync((Note)null).Verifiable();
            
            var result = await _notesController.PartiallyUpdateNote(Guid.NewGuid(), new JsonPatchDocument<NoteForPatchDto>()).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<NotFoundResult>(result);
        }

        [Test]
        public async Task PatchSuccessTest()
        {
            _mockNoteCollectionService.Setup(x => x.NoteExists(It.IsAny<Guid>())).ReturnsAsync(true).Verifiable();
            _mockNoteCollectionService.Setup(x => x.Get(It.IsAny<Guid>())).ReturnsAsync(new Note()).Verifiable();
            JsonPatchDocument<NoteForPatchDto> patchDoc = new JsonPatchDocument<NoteForPatchDto>();
            patchDoc.Replace(e => e.Name, "nume de test");
            patchDoc.Replace(e => e.Category, "categorie de test");

            var result = await _notesController.PartiallyUpdateNote(Guid.NewGuid(), new JsonPatchDocument<NoteForPatchDto>()).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<NoContentResult>(result);
        }

        [Test]
        public async Task PatchWhenInvalidModelTest()
        {
            JsonPatchDocument<NoteForPatchDto> patchDoc = new JsonPatchDocument<NoteForPatchDto>();
            patchDoc.Replace(e => e.Name, "name");
            patchDoc.Replace(e => e.Category, "name");
            _mockNoteCollectionService.Setup(x => x.NoteExists(It.IsAny<Guid>())).ReturnsAsync(true).Verifiable();
            _mockNoteCollectionService.Setup(x => x.Get(It.IsAny<Guid>())).ReturnsAsync(new Note());

            var result = await _notesController.PartiallyUpdateNote(Guid.NewGuid(), patchDoc);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<NotesAPI.Helpers.UnprocessableEntityObjectResult>(result);
        }

        [Test]
        public async Task PatchWhenNameAndCategoryAreEqualTest()
        {
            JsonPatchDocument<NoteForPatchDto> patchDoc = new JsonPatchDocument<NoteForPatchDto>();
            patchDoc.Replace(e => e.Name, "name");
            patchDoc.Replace(e => e.Category, "name");
            _mockNoteCollectionService.Setup(x => x.NoteExists(It.IsAny<Guid>())).ReturnsAsync(true).Verifiable();
            _mockNoteCollectionService.Setup(x => x.Get(It.IsAny<Guid>())).ReturnsAsync(new Note());

            var result = await _notesController.PartiallyUpdateNote(Guid.NewGuid(), patchDoc);

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<NotesAPI.Helpers.UnprocessableEntityObjectResult>(result);

            var objResult = (NotesAPI.Helpers.UnprocessableEntityObjectResult)result;
            var serializableError = (SerializableError)objResult.Value;
            var key = nameof(NoteForPatchDto);

            Assert.IsTrue(serializableError.ContainsKey(key));
            Assert.AreEqual((serializableError[key] as string[])[0], "The provided name should be different from the category");
        }
    }
}