using RestSharp;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using NotesAPI.Entities;

namespace APITest
{
    public class Tests
    {
        private RestClient _client;
        [SetUp]
        public void Setup()
        {
        }

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _client = new RestClient("https://localhost:44325");
        }

        [Test]
        public async Task GetNotesWithInexistingFieldsTest()
        {
            var request = new RestRequest("Notes?Fields=asdf", Method.Get);
            var result = await _client.ExecuteAsync(request);

            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        }

        [Test]
        public async Task GetNotesWhenValidFieldsTest()
        {
            var request = new RestRequest("Notes?Fields=Id", Method.Get);
            var result = await _client.ExecuteAsync(request);

            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task GetNotesWhenNoFieldsTest()
        {
            var request = new RestRequest("Notes", Method.Get);
            var result = await _client.ExecuteAsync(request);

            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task GetNoteThatDoesNotExistTest()
        {
            var request = new RestRequest("Notes/3fa85f64-5717-4562-b3fc-2c963f66af00", Method.Get);
            var result = await _client.ExecuteAsync(request);

            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(result.Content, Is.EqualTo("\"There is no note with the given Id\""));
        }

        [Test]
        public async Task GetValidNoteTest()
        {
            var request = new RestRequest("Notes/10a85f64-5717-4562-b3fc-2c963f66afa6");
            var result = await _client.GetAsync(request);

            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task DeleteValidNoteTest()
        {
            var request = new RestRequest("Notes/3fa85f64-5717-4562-b3fc-2c963f66afa6", Method.Delete);
            var result = await _client.ExecuteAsync(request);

            Assert.IsNotNull(result);
            Assert.That(result.Content, Is.EqualTo("\"Note deleted\""));
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task DeleteInvalidNoteTest()
        {
            var request = new RestRequest("Notes/10a85f64-0000-4562-b3fc-2c963f66afa6", Method.Delete);
            var result = await _client.ExecuteAsync(request);

            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
            Assert.That(result.Content, Is.EqualTo("\"Note not found\""));

        }

        [Test]
        public async Task UpdateValidNoteTest()
        {
            var request = new RestRequest("Notes/10a85f64-5717-4562-b3fc-2c963f66afa6", Method.Put);
            request.AddJsonBody<Note>(new Note() { Id = Guid.NewGuid(), Title = "Titlu actualizat in ValidNote"});
            var result = await _client.ExecuteAsync(request);

            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(result.Content, Is.EqualTo("\"Update successful\""));
        }

        [Test]
        public async Task UpdateWhenInvalidNoteTest()
        {
            var request = new RestRequest("Notes/10a85f64-5717-4562-b3fc-2c963f66afa6", Method.Put);
            var result = await _client.ExecuteAsync(request);


            Assert.IsNotNull(result);
            Assert.That(result.StatusCode, Is.EqualTo(HttpStatusCode.UnsupportedMediaType));
        }
    }
}