﻿using NotesAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Services
{
    public interface INoteCollectionService : ICollectionService<Note>
    {
        Task<List<Note>> GetNotesByOwnerId(Guid ownerId);
        Task<List<Note>> GetAllByIds(IEnumerable<Guid> ids);
        Task<bool> NoteExists(Guid id);
        Task<string> GetCategoryForNote(Guid noteId, string id);
    }
}
