using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using NotesAPI.Helpers;
using NotesAPI.Services;
using NotesAPI.Settings;
using System;
using System.IO;
using System.Reflection;
using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;

namespace NotesAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
                //setupAction.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
            });

            services.Configure<MongoDBSettings>(Configuration.GetSection(nameof(MongoDBSettings)));
            services.AddSingleton<IMongoDBSettings>(sp => sp.GetRequiredService<IOptions<MongoDBSettings>>().Value);

            services.AddSwaggerGen((c) =>
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddControllers();
            services.AddSingleton<INoteCollectionService, NoteCollectionService>();
            services.AddSingleton<IOwnerCollectionService, OwnerCollectionService>();
            services.AddTransient<ITypeHelperService, TypeHelperService>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IUrlHelper>(implementationFactory =>
            {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>()
                .ActionContext;
                return new UrlHelper(actionContext);
            });
            //services.AddScoped<IUrlHelper, UrlHelper>();

            services.AddControllersWithViews()
            .AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            services.AddHttpCacheHeaders((expirationModelOptions) => { expirationModelOptions.MaxAge = 600; },
            (validationModelOptions) => { validationModelOptions.MustRevalidate = true; });

            services.AddResponseCaching();

            services.AddMemoryCache();
            //services.Configure<IpRateLimitOptions>((options) =>
            //{
            //    options.GeneralRules = new System.Collections.Generic.List<RateLimitRule>()
            //    {
            //        new RateLimitRule()
            //        {
            //            Endpoint = "*",
            //            Limit = 3,
            //            Period = "5m"
            //        }
            //    };
            //});
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IProcessingStrategy, AsyncKeyLockProcessingStrategy>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseIpRateLimiting();

            app.UseResponseCaching();
            app.UseHttpCacheHeaders();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = string.Empty;
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Notes Api V1");
            });


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An unexpected fault happened. Try again later.");
                    });
                });
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Entities.Note, Models.NoteDto>()
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => CategoryExtension.ConvertCategory(src.CategoryId)))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Title));

                cfg.CreateMap<Models.NoteForUpdateDto, Entities.Note>();

                cfg.CreateMap<Models.NoteForPatchDto, Entities.Note>()
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => CategoryExtension.ConvertCategory(src.Category)))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Name));

                cfg.CreateMap<Entities.Note, Models.NoteForPatchDto>()
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => CategoryExtension.ConvertCategory(src.CategoryId)))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Title));
            });

        }
    }
}
