﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NotesAPI.Models
{
    public abstract class NoteManipulationDto
    {
        [Required]
        public Guid Id { get; set; }
        [MinLength(3, ErrorMessage = "enter an input bigger then len 3")]
        public virtual string Name { get; set; }
        public virtual string Category { get; set; }
    }
}
