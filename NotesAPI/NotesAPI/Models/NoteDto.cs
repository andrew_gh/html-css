﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NotesAPI.Models
{
    public class NoteDto : LinkResourcebaseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
