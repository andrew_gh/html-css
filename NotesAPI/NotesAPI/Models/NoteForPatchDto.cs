﻿namespace NotesAPI.Models
{
    public class NoteForPatchDto
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
