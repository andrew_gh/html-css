﻿namespace NotesAPI.Models
{
    public class NoteForUpdateDto
    {
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
