﻿using System.Collections;
using System.Collections.Generic;

namespace NotesAPI.Models
{
    public class LinkedCollectionResourceWrapperDto<T> :LinkResourcebaseDto
        where T : LinkResourcebaseDto
    {
        public IEnumerable<T> Value { get; set; }

        public LinkedCollectionResourceWrapperDto(IEnumerable<T> value)
        {
            Value = value;
        }
    }
}
