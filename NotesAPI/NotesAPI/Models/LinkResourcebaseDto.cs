﻿using System.Collections.Generic;

namespace NotesAPI.Models
{
    public abstract class LinkResourcebaseDto
    {
        public List<LinkDto> Links { get; set; } = new List<LinkDto>();
    }
}
