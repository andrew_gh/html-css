﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Models
{
    public class Category
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Id { get; set; }


    }

}
