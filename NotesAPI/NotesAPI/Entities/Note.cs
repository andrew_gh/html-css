﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Entities
{
    public class Note
    {
        public Guid OwnerId { get; set; }
        [Required (ErrorMessage ="Id Error")]
        public Guid Id { get; set; }
        [Required]
        [MinLength (4)]
        public string Title { get; set; }
        public string Description { get; set; }
        public string CategoryId { get; set; }

    }
}
