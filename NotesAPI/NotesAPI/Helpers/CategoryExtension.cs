﻿namespace NotesAPI.Helpers
{
    public static class CategoryExtension
    {
        public static string ConvertCategory(string categ="")
        {
            string categoryName = "not in a category";
            switch (categ)
            {
                case "1":
                    categoryName = "categoria 1";
                    break;
                case "2":
                    categoryName = "categoria 2";
                    break;
                case "3":
                    categoryName = "categoria 3";
                    break;
                default:
                    break;
            }
            return categoryName;
        }
    }
}
