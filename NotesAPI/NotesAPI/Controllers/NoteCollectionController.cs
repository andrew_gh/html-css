﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using NotesAPI.Entities;
using NotesAPI.Helpers;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NoteCollectionController : Controller
    {
        private INoteCollectionService _noteCollectionService;

        public NoteCollectionController(INoteCollectionService collectionService)
        {
            _noteCollectionService = collectionService;
        }

        [HttpPost]
        public IActionResult Create(IEnumerable<Note> noteCollection)
        {
            if (noteCollection == null)
            {
                return BadRequest();
            }

            foreach (var note in noteCollection)
            {
                _noteCollectionService.Create(note);
            }

            var notes = AutoMapper.Mapper.Map<IEnumerable<NoteDto>>(noteCollection);
            var idsAsString = string.Join(",", notes.Select(a => a.Id));

            return CreatedAtRoute("GetAuthorCollection", new { ids = idsAsString }, noteCollection);
        }

        [HttpGet("({ids})", Name = "GetAuthorCollection")]
        public IActionResult GetNoteCollection([FromBody][ModelBinder(BinderType = typeof(ArrayModelBinder))] IEnumerable<Guid> ids)
        {
            if (ids == null)
            {
                return BadRequest();
            }

            var noteEntities = _noteCollectionService.GetAllByIds(ids);

            if (ids.Count() != noteEntities.Result.Count())
            {
                return NotFound();
            }

            return Ok(noteEntities.Result);
        }
    }
}
