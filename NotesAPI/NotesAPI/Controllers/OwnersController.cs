﻿using Microsoft.AspNetCore.Mvc;
using NotesAPI.Entities;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OwnersController : ControllerBase
    {
        private IOwnerCollectionService _ownerCollectionService;

        public OwnersController(IOwnerCollectionService ownerCollectionService)
        {
            _ownerCollectionService = ownerCollectionService ?? throw new ArgumentNullException(nameof(ownerCollectionService));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] Owner owner)
        {
            if (owner == null)
                return BadRequest("Owner can not be null");

            var ok = await _ownerCollectionService.Update(id, owner);

            if (!ok)
                return Ok("Update successful");
            return NotFound("There is no owner with the given Id");
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            bool ok = await _ownerCollectionService.Delete(id);
            if (!ok)
                return NoContent();
            return NotFound("There is no owner with given id");
        }

        [HttpGet("{id}", Name = "GetOwner")]
        public async Task<IActionResult> Get(Guid id)
        {
            Owner owner = await _ownerCollectionService.Get(id);
            if (owner == null)
                return NotFound("There is no owner with the given Id");
            return Ok(owner);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            List<Owner> owners = await _ownerCollectionService.GetAll();
            if(owners == null)
                return BadRequest("no owners");
            return Ok(owners);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Owner owner)
        {
            if (owner == null)
            {
                return BadRequest("Owner cannot be null");
            }
            _ownerCollectionService.Create(owner);
            return CreatedAtRoute("GetOwner", new { id = owner.Id }, owner);
        }
    }
}
