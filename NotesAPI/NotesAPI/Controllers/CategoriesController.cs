﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        static List<Category> _categories = new List<Category> {
        new Category{Name="To Do", Id="1"},
        new Category{ Name= "Done", Id= "2"},
        new Category{ Name= "Doing", Id= "3"}
        };

        /// <summary>
        /// Gets the list of categories
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_categories);
        }

        /// <summary>
        /// Gets the category with the specified id
        /// </summary>
        /// <param name="id">The id is the unique identifier for a category</param>
        /// <returns>The wanted object</returns>
        [HttpGet("{id}", Name = "GetCategory")]
        public IActionResult GetOne(string id)
        {
            var category = _categories.Where((category) => category.Id == id);
            if (category == null)
            {
                return NotFound("Id not found");
            }
            return Ok(category);
        }

        /// <summary>
        /// Creates a new category
        /// </summary>
        /// <param name="category"> The name seen by the user of a certain category</param>
        [HttpPost]
        public IActionResult CreateCategory([FromBody] Category category)
        {
            if (category == null)
            {
                return BadRequest("Note cannot be null");
            }

            _categories.Add(category);
            return CreatedAtRoute("GetCategory", new { id = category.Id }, category);
        }

        /// <summary>
        /// Deletes a category based on given Id
        /// </summary>
        /// <param name="id">The id is the unique identifier for a category</param>
        /// <returns>The updated list of categories</returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteCategory(string id)
        {
            int index = _categories.FindIndex((category) => category.Id == id);
            if(index==-1)
                return NotFound("Id not found");
            _categories.RemoveAt(index);
            return Ok(_categories);
        }
    }
}
