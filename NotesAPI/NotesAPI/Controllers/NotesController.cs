﻿using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using NotesAPI.Entities;
using NotesAPI.Helpers;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]

    public class NotesController : ControllerBase
    {
        private INoteCollectionService _noteCollectionService;
        private ITypeHelperService _typeHelperService;
        private IUrlHelper _urlHelper;
        private readonly IMapper _mapper;
        public NotesController(INoteCollectionService noteCollectionService, ITypeHelperService typeHelperService,
            IUrlHelperFactory urlHelper, IActionContextAccessor actionContextAccessor/*, IMapper mapper*/)
        {
            _noteCollectionService = noteCollectionService ?? throw new ArgumentNullException(nameof(noteCollectionService));
            _typeHelperService = typeHelperService;
            _urlHelper = urlHelper.GetUrlHelper(actionContextAccessor.ActionContext);
            //_mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpPost(Name = "PostNote")]
        public async Task<IActionResult> Create(Note note)
        {
            if (note == null)
            {
                return BadRequest("Note cannot be null");
            }

            await _noteCollectionService.Create(note);
            return CreatedAtRoute("GetNote", new { id = note.Id }, note);
        }

        [HttpGet(Name = "GetNotes")]
        [HttpHead]
        public async Task<IActionResult> GetAll([FromQuery] NoteResourceParameters notesResourceParameters = null)
        {
            List<Note> notesFromRepo = await _noteCollectionService.GetAll();

            if (notesResourceParameters != null)
            {
                if(!_typeHelperService.TypeHasProperties<NoteDto>(notesResourceParameters.Fields))
                    return BadRequest();
            }

            var notes = AutoMapper.Mapper.Map<IEnumerable<NoteDto>>(notesFromRepo);
            var wrapper = new LinkedCollectionResourceWrapperDto<NoteDto>(notes);

            ////pentru reshape, peste ce exista deja, trebuie adaugat la CreateLinksForNotes sa dea support la ExpandoObject
            return Ok(CreateLinksForNotes(wrapper/*.Value.ShapeData(notesResourceParameters.Fields)*/));
        }

        [HttpGet("{id}", Name = "GetNote")]
        public async Task<IActionResult> Get(Guid id)
        {
            var noteFromRepo = await _noteCollectionService.Get(id);
            //var a = new Random();
            //var randNum = a.Next(101);
            //if (randNum < 25 && randNum > 75)
            //    throw new Exception("random exception for testing");

            if (noteFromRepo == null)
                return NotFound("There is no note with the given Id");
            var note = AutoMapper.Mapper.Map<NoteDto>(noteFromRepo);

            return Ok(CreateLinksForNote(note));
        }

        [HttpDelete("{id}", Name = "NoteDelete")]
        public async Task<IActionResult> Delete(Guid id)
        {
            bool ok = await _noteCollectionService.Delete(id);
            if (!ok)
            {
                return NotFound();
            }
            return Ok("Note deleted");
        }

        [HttpPut("{id}", Name = "NotePut")]
        public async Task<IActionResult> UpdateNote(Guid id, [FromBody] Note note)
        {
            if (note == null)
                return BadRequest("Note can not be null");
            
            await _noteCollectionService.Update(id, note);

            return Ok("Update successful");
        }

        [HttpPatch("{id}", Name = "NotePatch")]
        public async Task<IActionResult> PartiallyUpdateNote(Guid noteId, [FromBody] JsonPatchDocument<NoteForPatchDto> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            if (!await _noteCollectionService.NoteExists(noteId))
            {
                return NotFound();
            }

            var noteFromRepo = await _noteCollectionService.Get(noteId);

            if (noteFromRepo == null)
            {
                return NotFound();
            }

            var noteToPatch = AutoMapper.Mapper.Map<NoteForPatchDto>(noteFromRepo);

            patchDoc.ApplyTo(noteToPatch);

            if (noteToPatch.Name == noteToPatch.Category)
            {
                ModelState.AddModelError(nameof(NoteForPatchDto), "The provided name should be different from the category");
            }

            TryValidateModel(noteToPatch);

            if (!ModelState.IsValid)
            {
                return new Helpers.UnprocessableEntityObjectResult(ModelState);
            }

            AutoMapper.Mapper.Map(noteToPatch, noteFromRepo);

            await UpdateNote(noteFromRepo.Id, noteFromRepo);

            return NoContent();
        }

        [HttpOptions]
        public IActionResult GetNotesOptions()
        {
            Response.Headers.Add("Allow", "GET,OPTIONS,POST");
            return Ok();
        }

        private NoteDto CreateLinksForNote(NoteDto note)
        {
            note.Links.Add(new LinkDto(_urlHelper.Link("GetNote", new { note.Id }), "self", "GET"));
            return note;
        }

        private LinkedCollectionResourceWrapperDto<NoteDto> CreateLinksForNotes(
            LinkedCollectionResourceWrapperDto<NoteDto> booksWrapper)
        {
            booksWrapper.Links.Add(new LinkDto(_urlHelper.Link("GetNotes", new { }), "self", "GET"));
            return booksWrapper;
        }
    }
}
