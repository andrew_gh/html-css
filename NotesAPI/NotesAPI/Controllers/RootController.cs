﻿using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using System.Collections.Generic;

namespace NotesAPI.Controllers
{
    [Route("api")]
    public class RootController : Controller
    {
        private IUrlHelper _urlHelper;
        public RootController(IUrlHelper url)
        {
            _urlHelper = url;
        }

        [HttpGet(Name = "GetRoot")]
        public IActionResult GetRoot([FromHeader(Name = "Accept")] string mediaType)
        {
            if(mediaType == "application/vnd.marvin.hateoas+json")
            {
                var links = new List<LinkDto>();

                links.Add(new LinkDto(_urlHelper.Link("GetRoot", new { }), "self", "GET"));

                links.Add(new LinkDto(_urlHelper.Link("GetNotes", new { }), "notes", "GET"));

                links.Add(new LinkDto(_urlHelper.Link("PostNote", new { }), "create_note", "GET"));

                return Ok(links);
            }

            return NoContent();
        }
    }
}
