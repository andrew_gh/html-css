import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'notes-app';
  text:string ="text"
  dataTest: Date = new Date(21,1,2022);
  myNumber: number = 10;
}
