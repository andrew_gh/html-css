import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { App2Component } from './app2/app2.component';
import { DummyModule } from './dummy/dummy.module';
import { WaterfallModule } from './waterfall/waterfall.module';
import { AddValuePipe } from './add-value.pipe';
import { StringModifPipe } from './app2/string-modif.pipe';
import { MyDirectiveDirective } from './app2/my-directive.directive';
import { RoutingExerciseComponent } from './routing-exercise/routing-exercise.component';
import { RoutingEx2Component } from './routing-ex2/routing-ex2.component';


@NgModule({
  declarations: [
    AppComponent,
    App2Component,
    AddValuePipe,
    StringModifPipe,
    MyDirectiveDirective,
    RoutingExerciseComponent,
    RoutingEx2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DummyModule,
    WaterfallModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
