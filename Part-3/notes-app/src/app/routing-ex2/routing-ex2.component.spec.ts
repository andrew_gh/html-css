import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingEx2Component } from './routing-ex2.component';

describe('RoutingEx2Component', () => {
  let component: RoutingEx2Component;
  let fixture: ComponentFixture<RoutingEx2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutingEx2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingEx2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
