import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-routing-ex2',
  templateUrl: './routing-ex2.component.html',
  styleUrls: ['./routing-ex2.component.scss']
})
export class RoutingEx2Component implements OnInit {

  idRecieved;

  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(parameter => { 
      this.idRecieved=parameter["messege"]
    })
  }

}
