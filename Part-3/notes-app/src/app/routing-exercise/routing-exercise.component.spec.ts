import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingExerciseComponent } from './routing-exercise.component';

describe('RoutingExerciseComponent', () => {
  let component: RoutingExerciseComponent;
  let fixture: ComponentFixture<RoutingExerciseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutingExerciseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
