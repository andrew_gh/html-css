import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringModif'
})
export class StringModifPipe implements PipeTransform {

  transform(value: string, content?: string): string {
    return (content)? "-"+content+"-" : value;
  }

}
