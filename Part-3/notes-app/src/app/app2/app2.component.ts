import { Component, OnInit } from '@angular/core';
import { MyElement } from '../my-element';

@Component({
  selector: 'app-app2',
  templateUrl: './app2.component.html',
  styleUrls: ['./app2.component.scss']
})
export class App2Component implements OnInit {

  elements: MyElement[] = [
    {
      id: 0,
      content:"content nr 0",
      date:new Date("2019-01-16")
    },
    {
      id: 1,
      content:"content nr 1",
      date:new Date("2022-01-16")
    },
    {
      id: 2,
      content:"content nr 2",
      date:new Date("2019-01-16")
    },
    {
      id: 3,
      content:"content nr 3",
      date:new Date("2019-01-16")
    },
    {
      id: 4,
      content:"content nr 4",
      date:new Date("2019-01-16")
    },
    {
      id: 5,
      content:"content nr 5",
      date:new Date("2023-01-16")
    }
  ];

  compareDates(date1){
    if(date1 >= new Date("2022-01-01"))
      return true;
    else
      return false;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
