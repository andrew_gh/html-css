import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Dum1Component } from './dum1/dum1.component';
import { Dum2Component } from './dum2/dum2.component';




@NgModule({
  declarations: [
    Dum1Component,
    Dum2Component
  ],
  imports: [
    CommonModule
  ],
  exports:[Dum1Component,
    Dum2Component]
})
export class DummyModule { }
