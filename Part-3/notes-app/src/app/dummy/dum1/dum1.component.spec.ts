import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Dum1Component } from './dum1.component';

describe('Dum1Component', () => {
  let component: Dum1Component;
  let fixture: ComponentFixture<Dum1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Dum1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Dum1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
