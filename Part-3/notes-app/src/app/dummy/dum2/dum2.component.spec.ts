import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Dum2Component } from './dum2.component';

describe('Dum2Component', () => {
  let component: Dum2Component;
  let fixture: ComponentFixture<Dum2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Dum2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Dum2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
