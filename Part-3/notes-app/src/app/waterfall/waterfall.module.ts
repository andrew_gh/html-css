import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './m1/comp2/comp2.component';
import { M1Module } from './m1/m1.module';



@NgModule({
  declarations: [
    Comp1Component
  ],
  imports: [
    CommonModule,
    M1Module
  ],
  exports:[
    Comp1Component
  ]
})
export class WaterfallModule { }
