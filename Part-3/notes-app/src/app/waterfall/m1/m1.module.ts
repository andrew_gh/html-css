import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Comp2Component } from './comp2/comp2.component';
import { Comp3Component } from './m2/comp3/comp3.component';
import { M2Module } from './m2/m2.module';



@NgModule({
  declarations: [
    Comp2Component
  ],
  imports: [
    CommonModule,
    M2Module
  ],
  exports:[
    Comp2Component
  ]
})
export class M1Module { }
