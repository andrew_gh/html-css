import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutingEx2Component } from './routing-ex2/routing-ex2.component';
import { RoutingExerciseComponent } from './routing-exercise/routing-exercise.component';

const routes: Routes = [
  {path: "", component: RoutingExerciseComponent, pathMatch:'full'},
  {path: "app-routing-ex2/:messege", component: RoutingEx2Component},
  {path: "**", redirectTo: ""}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
