import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Category } from '../interfaces/category';
import { FiltersService } from '../services/filters.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  
  @Output() emitSelectedFilter = new EventEmitter<string>();

  categories:Category[] | undefined;
  
  constructor(private filtersService: FiltersService) { }

  selectFilter(categoryId: string | undefined) {
    this.emitSelectedFilter.emit(categoryId);
  }

  ngOnInit(): void {
    this.categories = this.filtersService.getCategories();
  }
}
