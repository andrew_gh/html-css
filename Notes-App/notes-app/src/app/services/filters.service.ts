import { Injectable } from '@angular/core';
import { Category } from '../interfaces/category';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {

  categories: Category[] = [
    {name:'To Do', id:'1'},
    {name:'Done', id:'2'},
    {name:'Doing', id:'3'}
  ];

  constructor() { }

  getCategories(): Category[] {
    return this.categories;
  }

  getCategoryById(id: string): Category|undefined{
      return this.categories.find((category)=> category.id === id);
  }
}