import { Injectable } from '@angular/core';
import { Note } from '../interfaces/note';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { catchError, Observable } from 'rxjs';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  readonly baseUrl= "https://localhost:4200";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };


  constructor(private httpClient: HttpClient) { }

  // serviceCall() {
  //   console.log("Note service was called");
  // }
  getNote(id: string):Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl+ `/notes`, this.httpOptions).pipe(map((notes:Note[]) =>
    { return notes.filter(note => note.id === id )} ));
  }

  getNotes():Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl + `/notes`, this.httpOptions);
  }

  getFilteredNotes(categId: string): Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.baseUrl + "/notes",this.httpOptions).pipe(map((notes:Note[]) =>
    { return notes.filter(note => note.categoryId === categId )} ));
  }

  getSearchedNotes(argSearchedText: string){
    return this.httpClient.get<Note[]>(this.baseUrl + "/notes",this.httpOptions).pipe(map((notes:Note[])=>
    notes.filter(nota => nota.title.toLocaleLowerCase().includes(argSearchedText.toLocaleLowerCase())
    || nota.description.toLocaleLowerCase().includes(argSearchedText.toLocaleLowerCase()) )))
  }

  addNote(note:Note){
    return this.httpClient.post(this.baseUrl + `/notes/`,note ,this.httpOptions);
  }

  editNote(note:Note){
    console.log(note.id);
    return this.httpClient.put(this.baseUrl + `/notes/`+ note.id ,note);
  }

  deleteNote(id: string): Observable<Note> {
    return this.httpClient.delete<Note>(this.baseUrl +`/${id}`);
  }

  // getNotes() {
  //   return this.notes;
  // }
  
  // addNote(note: Note) {
  //   this.notes.push(note);
  // }

  // getFiltredNotes(argCategoryId: string){
  //   return this.notes.filter(nota => nota.category === argCategoryId )
  // }

  // getSearchedNotes(argSearchedText: string){
  //   console.log("da");
  //   return this.notes.filter(nota => nota.title.toLocaleLowerCase().includes(argSearchedText.toLocaleLowerCase())
  //    || nota.description.toLocaleLowerCase().includes(argSearchedText.toLocaleLowerCase()) )
  // }
}
