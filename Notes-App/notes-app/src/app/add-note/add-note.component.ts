import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router'
import { Category } from '../interfaces/category';
import { Note } from '../interfaces/note';
import { FiltersService } from '../services/filters.service';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {

  noteTitle: string | undefined;
  noteDescription: string | undefined;
  categories: Category[] | undefined;
  selectedCategoryId: string | undefined;

  parameter:string;
  notes:Note[];

  
  constructor(private noteService: NoteService,private _router: Router, private _activatedRoute: ActivatedRoute, private filterService: FiltersService) { }

  ngOnInit(): void {
    this.categories = this.filterService.getCategories();
    
    this._activatedRoute.params.subscribe(parameter => {
      this.parameter=parameter["id"];
      if(this.parameter != "add"){
        this.noteService.getNote(this.parameter).subscribe((notes:Note[]) => {this.notes=notes } );
        this.noteTitle=this.notes[0].title;
        this.noteDescription=this.notes[0].description;
        this.selectedCategoryId=this.notes[0].categoryId;
      }
    })

    this._activatedRoute.queryParams.subscribe(queryParams => {
      // console.log(queryParams["title"]);
      // console.log(queryParams["description"]);
    })
  }

  editNote(){
    const note: Note = {
      id: this.parameter,
      title: this.noteTitle,
      description: this.noteDescription,
      categoryId: this.selectedCategoryId
    }
    this.noteService.editNote(note).subscribe();
    this.noteService.getNotes().subscribe((notes) => {this.notes=notes})
    this._router.navigate(['']);
  }

  addNote(){
    const note: Note = {
      id: Math.random().toString(36).substring(7),
      title: this.noteTitle,
      description: this.noteDescription,
      categoryId: this.selectedCategoryId
    }

    this.noteService.addNote(note).subscribe();
    

    // let noteToAdd: Note = {
    //   id: Math.random().toString(36).substring(7),
    //   title: this.noteTitle,
    //   description: this.noteDescription,
    //   category: this.selectedCategoryId,
    // }

    // this.noteService.addNote(noteToAdd);
    this._router.navigate(['']);
  }
}
