import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Note } from '../interfaces/note';
import { NoteService } from '../services/note.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit , OnChanges{
  
  @Input() selectedCategoryId: string;

  @Input() searchedText: string;
  notes: Note[];

  constructor(private service: NoteService,private _router: Router) { }

  editNote(id:string){
    this._router.navigate([`note-view/${id}`]);
  }

  deleteNote(idToBeDeleted:string){
    this.service.deleteNote(idToBeDeleted).subscribe();
    this.service.getNotes().subscribe((notes) => {this.notes=notes})
  }

  ngOnInit(): void {
    this.service.getNotes().subscribe((notes:Note[]) => {this.notes=notes } );
  }

  ngOnChanges(): void {
    if(this.selectedCategoryId != "")
      this.service.getFilteredNotes(this.selectedCategoryId).subscribe((notes:Note[]) => {this.notes=notes } );

    if(this.searchedText != "")
      this.service.getSearchedNotes(this.searchedText).subscribe((notes:Note[]) => {this.notes=notes } )
  }


  

  // ngOnInit(): void {
  //   this.service.serviceCall();
  //   this.notes=this.service.getNotes();
  // }

  // ngOnChanges(): void{
  //   console.log(this.selectedCategoryId) 
  //   if(this.selectedCategoryId != "")
  //      this.notes = this.service.getFiltredNotes(this.selectedCategoryId);
  //   console.log(this.searchedText)
  //   if(this.searchedText != "")
  //     this.notes = this.service.getSearchedNotes(this.searchedText)
  // }

}
function idToBeDeleted(idToBeDeleted: any) {
  throw new Error('Function not implemented.');
}

