import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {

  title:string= "Add note"
  titleColor:string ="red"
  noteContent:string= ""
  titleBackgroundColor:string=""
  color:string=""


  setTitle() { this.noteContent = "modificare" ; }
  
  constructor(private _router: Router) { }

  
  toAddNote(){
    this._router.navigate([`note-view/add`]);
  }

  ngOnInit(): void {
  }

}
